from django.contrib import admin
from .models import GeneratedWord

# Register your models here.


class WordAdmin(admin.ModelAdmin):
    pass


admin.site.register(GeneratedWord, WordAdmin)