from django.db import models

# Create your models here.


class GeneratedWord(models.Model):
    generated_word = models.CharField(max_length=30)
    published = models.DateTimeField(auto_now_add=True)
    vote_up = models.IntegerField(default=0)
    vote_down = models.IntegerField(default=0)

    def __str__(self):
        return self.generated_word
