from django.shortcuts import render
from django.views.generic import ListView
from .models import GeneratedWord


class GeneratedWordsView(ListView):
    model = GeneratedWord
    template_name = "generator/gen_index.html"
    context_object_name = "word_list"
    paginate_by = 10

    def get_queryset(self):
        return GeneratedWord.objects.all()


def generate(request):
    return render(request, "generator/generate.html")
