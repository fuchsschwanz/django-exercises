from django.urls import path
from . import views


urlpatterns = [
    path("", views.GeneratedWordsView.as_view(), name="gen_index"),
    path("generate/", views.generate, name="generate")
]